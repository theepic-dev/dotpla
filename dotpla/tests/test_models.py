"""Tests for the dotpla models."""

from dotpla.models import BaseOTPLAModel


class TestDOTPLAModels:
    def test_generate_random_pass_length(self):
        assert (
            len(BaseOTPLAModel.generate_random_password()) == 36
        ), "Generated password should be 36 characters long, as it is UUID4."
