from django.apps import AppConfig


class DotplaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dotpla"
